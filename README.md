## 该仓库是ShopTNT-b2c商城的配置文件仓库

## 文档： http://docs.shoptnt.cn

## 使用须知

1. 允许个人学习使用
2. 允许用于学习、毕业设计等
3. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 

## 交流、反馈
官方QQ群：
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=RtRIL7WomrO79uDDDp4HihX_GH1xMIPD&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="ShopTNT开源交流群" title="ShopTNT开源交流群"></a> 766503360

## b2c项目地址：

Java后端：https://gitee.com/shoptnt/shop.git

PC端：https://gitee.com/shoptnt/shoptnt-ui.git

配置中心：https://gitee.com/shoptnt/shoptnt-config.git

## B2B2C项目开源地址

后台API：https://gitee.com/bbc-se/api

前端ui：https://gitee.com/bbc-se/pc-ui

移动端：https://gitee.com/bbc-se/mobile-ui

配置中心：https://gitee.com/bbc-se/config

